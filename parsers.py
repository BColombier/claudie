#!/usr/bin/python3.8

import dateutil
import fitparse
from gpx_converter import Converter
import os
import pandas as pd
import tcxparser

COLUMNS = ["Timestamp", "Latitude", "Longitude"]


def sem_to_deg(s):
    return s*180.0/2**31


def get_fit_timestamp(config, fitfile_name):
    try:
        fitfile = fitparse.FitFile(os.path.join(config["general"]["activities_directory"],
                                                fitfile_name))
    except fitparse.utils.FitHeaderError as e:
        raise Exception("Error parsing fit file {}".format(fitfile_name)) from e
    timestamp = False
    for message in fitfile.get_messages():
        try:
            timestamp = int(message.get_values()["timestamp"].timestamp())
            break
        except KeyError:
            pass
    return timestamp


def fitfile_to_df(config, fitfile_name):
    raw_data = []
    try:
        fitfile = fitparse.FitFile(os.path.join(config["general"]["activities_directory"],
                                                fitfile_name))
    except fitparse.utils.FitHeaderError as e:
        raise Exception("Error parsing fit file {}".format(fitfile_name)) from e

    timestamp = get_fit_timestamp(config, fitfile_name)
    for message in fitfile.get_messages():
        try:
            raw_data.append((timestamp,
                             sem_to_deg(message.get_values()["position_lat"]),
                             sem_to_deg(message.get_values()["position_long"])))
        except KeyError:
            pass
    return pd.DataFrame(raw_data, columns=COLUMNS)


def gpxfile_to_df(config, gpxfile_name):
    try:
        gpxfile = os.path.join(config["general"]["activities_directory"], gpxfile_name)
        df = Converter(input_file=gpxfile).gpx_to_dataframe()
        del df['altitude']
        df = df.rename(columns={'time': 'Timestamp', 'latitude': 'Latitude', 'longitude': 'Longitude'})
        df['Timestamp'] = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
    except:
        raise Exception("Error parsing gpx file {}".format(gpxfile_name))
    return df


def tcxfile_to_df(config, tcxfile_name):
    try:
        tcxfile = os.path.join(config["general"]["activities_directory"], tcxfile_name)
        # First remove spaces
        with open(tcxfile, 'r+') as f:
            txt = f.read().lstrip(' ')
            f.seek(0)
            f.write(txt)
            f.truncate()
        tcx = tcxparser.TCXParser(tcxfile).activity
        df = pd.DataFrame(activities_to_array(tcx))
        df['Timestamp'] = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
    except:
        raise Exception("Error parsing tsv file {}".format(tcxfile_name))
    return df


def activities_to_array(activity):
    return_array = []
    for laps in activity.Lap:
        for tracks in laps.Track:
            for trackingpoints in tracks.Trackpoint:
                dictio = {'Timestamp': dateutil.parser.parse(str(trackingpoints.Time)),
                          'Latitude': float(trackingpoints.Position.LatitudeDegrees),
                          'Longitude': float(trackingpoints.Position.LongitudeDegrees)}
                return_array.append(dictio)
            return return_array
