#!/usr/bin/python3.8

from argparse import ArgumentParser
import configparser
import folium
from gpx_converter import Converter
import numpy as np
import pandas as pd
import os
import tcxparser
from tqdm import tqdm

import parsers


def build_database(config):
    raw_data = []
    for file_name in tqdm(sorted(os.listdir(config["general"]["activities_directory"]))):
        if file_name.lower().endswith(".fit"):
            try:
                raw_data.append(parsers.fitfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.lower().endswith(".gpx"):
            try:
                raw_data.append(parsers.gpxfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.lower().endswith(".tcx"):
            try:
                raw_data.append(parsers.tcxfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))

    data = pd.concat(raw_data,
                     ignore_index=True)
    data.to_pickle(config["general"]["database_name"])


def draw_map(config):
    data = pd.read_pickle(config["general"]["database_name"]).iloc[::int(config["general"]["decimate"]), :]  # Keep out_of_N row out of N
    print("Loaded data of shape {}".format(data.shape))
    the_map = folium.Map(location=[data.Latitude.median(), data.Longitude.median()],
                         zoom_start=10,
                         tiles="https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png",
                         attr="CyclOSM")
    for activity in tqdm(data["Timestamp"].unique()):
        activity_data = data[data["Timestamp"] == activity].drop("Timestamp", axis=1)
        folium.PolyLine([tuple(r) for r in activity_data.to_numpy()],
                        color=config["style"]["color"],
                        opacity=float(config["style"]["opacity"]),
                        weight=int(config["style"]["width"])).add_to(the_map)
    the_map.save(config["general"]["map_name"])


def update_database(config):
    try:
        existing_data = pd.read_pickle(config["general"]["database_name"])
    except FileNotFoundError as e:
        raise Exception("Could not find the database {} to update".format(config["general"]["database_name"])) from e
    raw_new_data = []
    for file_name in tqdm(sorted(os.listdir(config["general"]["activities_directory"]))):
        if file_name.lower().endswith(".fit"):
            try:
                if parsers.get_fit_timestamp(config, file_name) not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(parsers.fitfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.lower().endswith(".gpx"):
            try:
                gpxfile = os.path.join(config["general"]["activities_directory"], file_name)
                df = Converter(input_file=gpxfile).gpx_to_dataframe()
                df = df.rename(columns={'time': 'Timestamp', 'latitude': 'Latitude', 'longitude': 'Longitude'})
                timestamp = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
                if timestamp not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(parsers.gpxfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.lower().endswith(".tcx"):
            try:
                tcxfile = os.path.join(config["general"]["activities_directory"], file_name)
                # First remove spaces
                with open(tcxfile, 'r+') as f:
                    txt = f.read().lstrip(' ')
                    f.seek(0)
                    f.write(txt)
                    f.truncate()
                tcx = tcxparser.TCXParser(tcxfile).activity
                df = pd.DataFrame(parsers.activities_to_array(config, tcx))
                timestamp = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
                if timestamp not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(parsers.tcxfile_to_df(config, file_name))
            except Exception as e:
                tqdm.write(str(e))

    if raw_new_data:
        new_data = pd.concat(raw_new_data,
                             ignore_index=True)
        new_data = new_data.append(existing_data,
                                   ignore_index=True)
        new_data.to_pickle(config["general"]["database_name"])
    else:
        print("No update needed")


def stats(config):
    ROUNDING_PLACE = 4
    try:
        data = pd.read_pickle(config["general"]["database_name"])
    except FileNotFoundError as e:
        raise Exception("No database found, build it first by running: claudie build".format(config["general"]["database_name"])) from e
    data["Latitude_rounded"] = np.round(data["Latitude"], ROUNDING_PLACE)
    data["Longitude_rounded"] = np.round(data["Longitude"], ROUNDING_PLACE)
    unique_locs = data.drop_duplicates(subset=["Latitude_rounded", "Longitude_rounded"])
    data["been_here_before"] = 0
    print(len(unique_locs))


cmd_to_func = {
    "build": build_database,
    "draw": draw_map,
    "update": update_database,
    "stats": stats,
}

config = configparser.ConfigParser()
config.read("config.ini")

parser = ArgumentParser()
parser.add_argument("command",
                    type=str,
                    choices=[cmd for cmd in cmd_to_func.keys()])
args = parser.parse_args()

print("Command: {}".format(args.command))
cmd_to_func[args.command](config)
