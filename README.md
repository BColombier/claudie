# CLAUDIE

Voici Claudie : **C**artographie **L**ocale des **A**ctivités **U**tilisant les **D**onnées des **I**tinéraires **E**ffectués

# Pré-requis

Un dossier `activities` contenant les activités au format `.fit`, `.gpx` ou `.tcx`.

# Usage

## Construction de la base de données

`./claudie build`

## Mise à jour de la base de données

`./claudie update`

## Génération de la carte locale des activités

`./claudie draw`